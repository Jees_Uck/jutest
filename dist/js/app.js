/*
 * Copyright 2017 Google Inc. All rights reserved.
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
 * file except in compliance with the License. You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under
 * the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
 * ANY KIND, either express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

const mapStyle = [];

// Escapes HTML characters in a template literal string, to prevent XSS.
// See https://www.owasp.org/index.php/XSS_%28Cross_Site_Scripting%29_Prevention_Cheat_Sheet#RULE_.231_-_HTML_Escape_Before_Inserting_Untrusted_Data_into_HTML_Element_Content
function sanitizeHTML(strings) {
    const entities = {'&': '&amp;', '<': '&lt;', '>': '&gt;', '"': '&quot;', '\'': '&#39;'};
    let result = strings[0];
    for (let i = 1; i < arguments.length; i++) {
        result += String(arguments[i]).replace(/[&<>'"]/g, (char) => {
            return entities[char];
        });
        result += strings[i];
    }
    return result;
}

/**
 * Initialize the Google Map.
 */
function initMap() {
    // Create the map.
    const map = new google.maps.Map(document.getElementById('map'), {
        zoom: 7,
        center: {lat: 49.038575, lng: 31.449446},
        styles: mapStyle,
        mapTypeControl: false,
        panControl: false,
        zoomControl: false,
        streetViewControl: false,
        fullscreenControl: false

    });

    // Load the stores GeoJSON onto the map.
    map.data.loadGeoJson('stores.json', {idPropertyName: 'storeid'});

    // Define the custom marker icons, using the store's "category".
    map.data.setStyle((feature) => {
        return {
            icon: {
                url: `img/map/map_marker.svg`,
                scaledSize: new google.maps.Size(64, 64),
            },
        };
    });

    const apiKey = 'AIzaSyCfUCurMWJdJ-AZONZ2LXJeyAJl-juIhSg';
    const infoWindow = new google.maps.InfoWindow();

    document.getElementById('country').addEventListener(
        'change', setAutocompleteCountry);

    // Show the information for a store when its marker is clicked.
    map.data.addListener('click', (event) => {
        const category = event.feature.getProperty('category');
        const name = event.feature.getProperty('name');
        const city = event.feature.getProperty('city');
        const address = event.feature.getProperty('address');
        const hours = event.feature.getProperty('hours');
        const phone = event.feature.getProperty('phone');
        const email = event.feature.getProperty('email');
        const position = event.feature.getGeometry().get();
        const content = sanitizeHTML`
      <div class="info-window">
        <h2>${name}</h2><p>${address}, ${city}</p>
        <p><b>Open:</b> ${hours}<br/>${email}<br/><b>Phone:</b> ${phone}</p>
      </div>
      `;

        infoWindow.setContent(content);
        infoWindow.setPosition(position);
        infoWindow.setOptions({pixelOffset: new google.maps.Size(0, -30)});
        infoWindow.open(map);
    });

    // Build and add the search bar
    let card = document.createElement('div');
    let titleBar = document.createElement('div');
    let title = document.createElement('div');
    let container = document.createElement('div');
    let input = document.createElement('input');
    let options = {
        types: ['address'],
        componentRestrictions: {country: 'ua'},
    };

    card.setAttribute('id', 'pac-card');
    title.setAttribute('id', 'title');
    // title.textContent = 'Find the nearest store';
    titleBar.append(title);
    container.setAttribute('id', 'pac-container');
    input.setAttribute('id', 'pac-input');
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Enter an address');
    container.append(input);
    card.append(titleBar);
    card.append(container);
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(card);

    // Make the search bar into a Places Autocomplete search bar and select
    // which detail fields should be returned about the place that
    // the user selects from the suggestions.
    // const autocomplete = new google.maps.places.Autocomplete(input, options);

    const autocomplete = new google.maps.places.Autocomplete(input, options);

    autocomplete.setFields(
        ['address_components', 'geometry', 'name']);

    // Set the origin point when the user selects an address
    const originMarker = new google.maps.Marker({map: map});
    originMarker.setVisible(false);
    let originLocation = map.getCenter();

    autocomplete.addListener('place_changed', async () => {
        originMarker.setVisible(false);
        originLocation = map.getCenter();
        const place = autocomplete.getPlace();

        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert('No address available for input: \'' + place.name + '\'');
            return;
        }

        // Recenter the map to the selected address
        originLocation = place.geometry.location;
        map.setCenter(originLocation);
        map.setZoom(9);
        console.log(place);

        originMarker.setPosition(originLocation);
        originMarker.setVisible(true);

        // Use the selected address as the origin to calculate distances
        // to each of the store locations
        const rankedStores = await calculateDistances(map.data, originLocation);
        showStoresList(map.data, rankedStores);

        return;
    });
}

/**
 * Use Distance Matrix API to calculate distance from origin to each store.
 * @param {google.maps.Data} data The geospatial data object layer for the map
 * @param {google.maps.LatLng} origin Geographical coordinates in latitude
 * and longitude
 * @return {Promise<object[]>} n Promise fulfilled by an array of objects with
 * a distanceText, distanceVal, and storeid property, sorted ascending
 * by distanceVal.
 */
async function calculateDistances(data, origin) {
    const stores = [];
    const destinations = [];

    // Build parallel arrays for the store IDs and destinations
    data.forEach((store) => {
        const storeNum = store.getProperty('storeid');
        const storeLoc = store.getGeometry().get();

        stores.push(storeNum);
        destinations.push(storeLoc);
    });

    // Retrieve the distances of each store from the origin
    // The returned list will be in the same order as the destinations list
    const service = new google.maps.DistanceMatrixService();
    const getDistanceMatrix =
        (service, parameters) => new Promise((resolve, reject) => {
            service.getDistanceMatrix(parameters, (response, status) => {
                if (status != google.maps.DistanceMatrixStatus.OK) {
                    reject(response);
                } else {
                    const distances = [];
                    const results = response.rows[0].elements;
                    for (let j = 0; j < results.length; j++) {
                        const element = results[j];
                        const distanceText = element.distance.text;
                        const distanceVal = element.distance.value;
                        const distanceObject = {
                            storeid: stores[j],
                            distanceText: distanceText,
                            distanceVal: distanceVal,
                        };
                        distances.push(distanceObject);
                    }

                    resolve(distances);
                }
            });
        });

    const distancesList = await getDistanceMatrix(service, {
        origins: [origin],
        destinations: destinations,
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
    });

    distancesList.sort((first, second) => {
        return first.distanceVal - second.distanceVal;
    });

    return distancesList;
}

/**
 * Build the content of the side panel from the sorted list of stores
 * and display it.
 * @param {google.maps.Data} data The geospatial data object layer for the map
 * @param {object[]} stores An array of objects with a distanceText,
 * distanceVal, and storeid property.
 */
function showStoresList(data, stores) {
    if (stores.length == 0) {
        console.log('empty stores');
        return;
    }

    let panel = document.createElement('div');
    // If the panel already exists, use it. Else, create it and add to the page.
    if (document.getElementById('panel')) {
        panel = document.getElementById('panel');
        // If panel is already open, close it
        // if (panel.classList.contains('open')) {
        //     panel.classList.remove('open');
        // }
    } else {
        panel.setAttribute('id', 'panel');
        const body = document.body;
        body.insertBefore(panel, body.childNodes[0]);
    }


    // Clear the previous details
    while (panel.lastChild) {
        panel.removeChild(panel.lastChild);
    }

    stores.forEach((store) => {
        // Add store details with text formatting
        let wrapper = document.createElement('div');
        wrapper.classList.add('wrapper-card');
        let name = document.createElement('p');
        name.classList.add('place');
        let currentStore = data.getFeatureById(store.storeid);
        let city = document.createElement('p');
        city.textContent = currentStore.getProperty('city');
        city.classList.add('city');
        name.textContent = currentStore.getProperty('name');
        let address = document.createElement('p');
        address.textContent = currentStore.getProperty('address');
        let phone = document.createElement('p');
        phone.textContent = currentStore.getProperty('phone');
        let email = document.createElement('p');
        email.textContent = currentStore.getProperty('email');
        let hours = document.createElement('p');
        hours.textContent = currentStore.getProperty('hours');
        hours.classList.add('hours');
        panel.append(wrapper);
        wrapper.append(city, name, address, phone, email, hours);


    });

    // Open the panel
    // panel.classList.add('open');

    return;
}
function setAutocompleteCountry() {
    let countries = {
        'ua': {
            center: {lat: 49.0, lng: 31.4},
            zoom: 6
        },
        'by': {
            center: {lat: 53.9, lng: 27.6},
            zoom: 6
        },
    };
    let country = document.getElementById('country').value;
    if (country == 'all') {
        autocomplete.setComponentRestrictions({'country': []});
        map.setCenter({lat: 15, lng: 0});
        map.setZoom(2);
    } else {
        autocomplete.setComponentRestrictions({'country': country});
        map.setCenter(countries[country].center);
        map.setZoom(countries[country].zoom);
    }
}